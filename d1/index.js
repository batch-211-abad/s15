let supplier;
// This is considered as initialization because it is the first time that a 
// value has been assigned to a variable.
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// const pi;
pi = 3.1416;
console.log(pi);

a = 5;
console.log(a); //output: 5
var a;

const outerVariable = "Hello";

let country = "Philippines";
let province = "Metro Manila";

// Concatenating Strings

let fullAddress = province + ", " + country
console.log(fullAddress); //Metro Manila, Philippines

let greeting = "I live in the " + country;
console.log(greeting);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);
let grade = 98.6;

// Combining text and strings
console.log("John's grade last quarter is " + grade);

let isMarried = false;
let inGoodConduct = true;

console.log("is Married: " + isMarried);
console.log("in Good Conduct: " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that is used to store multiple values

// Arrays can store different data types but is normally used to store similar data types.

// similar data types
// Syntax
	// let/const arrayName = [elementA, elementB, element C, ....]

	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

	let details = ["John", "Smith", 32, true];
	console.log(details);

	// Objects
	// Objects are another special kind of data type that's used to mimic real world objects or items
	// They're used to create complex data that contains pieces of information that are relevant to each other.
	// Every individual piece of information is called a property of the object. 
	// Syntax
		// let/const objectName = {
			// property A : value,
			// property B: value,
		// }

		let person = {
			fullName: "Edward Scissorhands",
			age: 25,
			isMarried: false,
			contact:["+639171234567", "8123 4567"],
			address:{
				housenumber: '345',
				city: 'Manila'	
					}

			}
		
		console.log(person);

		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		}

		console.log(myGrades);

		// typeof operator

		console.log(typeof myGrades); //object
		console.log(typeof grades); //object
		// note: array is a special type of object with methods and functions to manipulate it
		// We will discuss these methods in later sessions (s22 - Array Manipulation)

		const anime = ['OP', 'OPM', 'AOT', 'BNHA'];

		anime[0] = ['JJK'];

		console.log(anime);


// Null

// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified.
let spouse = null;
console.log(spouse);
// null is also considered a data type of its own compared to 0 which is a NUMBER and single quotes which are a data type of a string

let myNumber = 0; //0
let myString = ''; //
console.log(myNumber);
console.log(myString);

// Undefined
// represents the state of a variable that has been declared but w/ 0 an assigned value


let fullName;
console.log(fullName);


// one clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.